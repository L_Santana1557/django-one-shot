from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm

# Create your views here.


def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "todo_list_list": lists,
    }
    return render(request, "todo_lists/list.html", context)


def todo_list_detail(request, id):
    list = TodoList.objects.get(id=id)
    context = {
        "todo_list_detail": list,
    }
    return render(request, "todo_lists/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoListForm()
    context = {
        "todo_list_create": form
    }

    return render(request, "todo_lists/create.html", context)


def todo_list_update(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm(instance=list)
    context = {
        "form": form,
    }
    return render(request, "todo_lists/update.html", context)
